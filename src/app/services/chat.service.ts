import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  constructor(public http: HttpClient) {}
  baseUrl = 'https://crudpi.io/f41ac6/schoolab';

  sendMessage(user, messageData) {
    const data = {
      username: user,
      message: messageData
    };

    return this.http.post(this.baseUrl, data);
  }

  getMessage() {
    return this.http.get(this.baseUrl);
  }
}
