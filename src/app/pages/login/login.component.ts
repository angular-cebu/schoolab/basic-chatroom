import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(public chatSvc: ChatService, public route: Router) {}
  isSignUp = false;
  isNotLoggedIn = true;
  errorMessage = '';

  accountCreateForm = new FormGroup({
    username: new FormControl('', [
      Validators.minLength(5),
      Validators.required
    ])
  });

  resetForm() {
    this.accountCreateForm.reset();
    this.errorMessage = '';
  }

  login() {
    if (this.accountCreateForm.valid) {
      localStorage.setItem(
        'username',
        this.accountCreateForm.getRawValue().username
      );

      this.route.navigate(['/feed']);
    }
  }

  ngOnInit() {}
}
