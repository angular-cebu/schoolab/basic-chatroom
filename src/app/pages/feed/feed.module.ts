import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeedRoutingModule } from './feed-routing.module';
import { FeedComponent } from './feed.component';
import { CardComponent } from './components/card/card.component';
import { MaterialModule } from 'src/app/material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [FeedComponent, CardComponent],
  imports: [
    CommonModule,
    FeedRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class FeedModule {}
