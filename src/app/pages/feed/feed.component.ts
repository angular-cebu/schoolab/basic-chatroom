import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { interval } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { Router } from '@angular/router';

export interface Feed {
  username: string;
  message: string;
}

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit, OnDestroy {
  constructor(public svc: ChatService, public route: Router) {}

  feed = new FormGroup({
    message: new FormControl('', [Validators.minLength(1), Validators.required])
  });

  feeds: any;
  updateSubscription: any;

  ngOnInit() {
    if (localStorage.getItem('username') === null) {
      this.route.navigate(['/login']);
      return;
    }

    this.updateSubscription = interval(3000)
      .pipe(startWith(0))
      .subscribe(() => {
        this.getFeeds();
      });
  }

  ngOnDestroy() {
    if (this.updateSubscription) {
      this.updateSubscription.unsubscribe();
    }
  }

  post() {
    this.svc
      .sendMessage(
        localStorage.getItem('username'),
        this.feed.getRawValue().message
      )
      .subscribe(response => {
        this.feed.reset();
        this.getFeeds();
      });
  }

  getFeeds() {
    this.svc.getMessage().subscribe(feeds => {
      this.feeds = feeds;
      this.feeds.reverse();
    });
  }

  logout() {
    localStorage.clear();
    this.route.navigate(['/login']);
  }
}
